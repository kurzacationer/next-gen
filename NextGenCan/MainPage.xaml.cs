﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Microcharts;
using SkiaSharp;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace NextGenCan
{
    /// <summary>
    ///     An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private readonly Database database;
        private readonly ThrashCan device;

        private string _currentBarcode;

        public MainPage()
        {
            InitializeComponent();

            SetupCharts();

            database = new Database();

            device = new ThrashCan();

            device.Send(CanCommand.CalibrateAll);
            device.Send(CanCommand.FillRateAll);

            SetupEvents();
        }

        #region Setup
        /// <summary>
        ///     Methode die de grafieken eenmalig opzetten.
        /// </summary>
        private void SetupCharts()
        {
            Chart1.Chart = new RadialGaugeChart
            {
                BackgroundColor = SKColor.Empty,
                IsAnimated = true,
                AnimationDuration = TimeSpan.FromMilliseconds(1000),
                MaxValue = 1,
                MinValue = 0,
                Entries = new[] { new ChartEntry(0), new ChartEntry(0), new ChartEntry(0), new ChartEntry(0) }
            };
            Chart2.Chart = new RadialGaugeChart
            {
                BackgroundColor = SKColor.Empty,
                IsAnimated = true,
                AnimationDuration = TimeSpan.FromMilliseconds(1000),
                MaxValue = 1,
                MinValue = 0,
                Entries = new[] { new ChartEntry(0), new ChartEntry(0), new ChartEntry(0), new ChartEntry(0) }
            };
            Chart3.Chart = new RadialGaugeChart
            {
                BackgroundColor = SKColor.Empty,
                IsAnimated = true,
                AnimationDuration = TimeSpan.FromMilliseconds(1000),
                MaxValue = 1,
                MinValue = 0,
                Entries = new[] { new ChartEntry(0), new ChartEntry(0), new ChartEntry(0), new ChartEntry(0) }
            };
            Chart4.Chart = new RadialGaugeChart
            {
                BackgroundColor = SKColor.Empty,
                IsAnimated = true,
                AnimationDuration = TimeSpan.FromMilliseconds(1000),
                MaxValue = 1,
                MinValue = 0,
                Entries = new[] { new ChartEntry(0), new ChartEntry(0), new ChartEntry(0), new ChartEntry(0) }
            };
            Chart5.Chart = new RadialGaugeChart
            {
                BackgroundColor = SKColor.Empty,
                IsAnimated = true,
                AnimationDuration = TimeSpan.FromMilliseconds(1000),
                MaxValue = 1,
                MinValue = 0,
                Entries = new[] { new ChartEntry(0), new ChartEntry(0), new ChartEntry(0), new ChartEntry(0) }
            };
            LargeChart.Chart = new BarChart
            {
                BackgroundColor = SKColor.Empty,
                IsAnimated = true,
                AnimationDuration = TimeSpan.FromMilliseconds(1000),
                MaxValue = 1,
                MinValue = 0
            };
            List1.MinHeight = 5;
        }

        /// <summary>
        ///     Methode waar alle events worden gelinked aan de juiste methode's
        /// </summary>
        private void SetupEvents()
        {
            BarcodeInput.KeyDown += BarcodeInputOnKeyDown;
            BarcodeInput.Loaded += (s, e) => { BarcodeInput.Focus(FocusState.Programmatic); };
            BarcodeInput.LostFocus += ForceFocus;

            device.OnFill += OnFill;
            device.OnPosition += OnPosition;
            device.OnPress += OnPress;
            device.OnBarcode += OnBarcode;
            device.OnFillAll += OnFillAll;
        }
        #endregion

        #region Interface
        /// <summary>
        ///     Methode die de grafieken bestuurd.
        /// </summary>
        /// <param name="material"></param>
        /// <param name="fillRate"></param>
        private void SetFillGraph(Material material, float fillRate)
        {
            Chart chart;
            switch (material)
            {
                case Material.Plastic:
                    chart = Chart1.Chart;
                    break;
                case Material.Paper:
                    chart = Chart2.Chart;
                    break;
                case Material.Deposit:
                    chart = Chart3.Chart;
                    break;
                case Material.Other:
                    chart = Chart4.Chart;
                    break;
                case Material.Green:
                    chart = Chart5.Chart;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(material), material, null);
            }

            var entries = chart.Entries.ToArray();
            var chartEntry = new ChartEntry(fillRate)
            {
                Color = MaterialColor.GetColor(material),
                Label = material.ToString(),
                ValueLabel = ((int)(fillRate * 100)).ToString()
            };
            entries[entries.Length - 1] = chartEntry;
            chart.Entries = entries;
        }
        #endregion

        #region Events
        /// <summary>
        ///     Deze methode zorgt ervoor dat de textview waar de barcode ingevoerd wordt constant gefocused is.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ForceFocus(object sender, RoutedEventArgs e)
        {
            BarcodeInput.Focus(FocusState.Programmatic);
        }


        /// <summary>
        ///     Event voor het verwerken van barcodes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBarcode(object sender, string e)
        {
            if (e == "") return;

            database.AddBarcode(e);

            var product = database.GetProduct(e);
            if (!product.Equals(default))
            {
                device.Send(CanCommand.Position, product.Material);
                List1.Items?.Insert(0, product);
                if (List1.Items?.Count > 5) List1.Items?.RemoveAt(List1.Items.Count - 1);
                BarcodeInput.Text = "";
            }
            else
                _currentBarcode = e;
        }

        /// <summary>
        ///     Methode die de enter van de barcode scanner verwerkt.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BarcodeInputOnKeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter) device.InvokeBarcode(this, BarcodeInput.Text);

            if (e.Key == VirtualKey.Control) OnPress(this, EventArgs.Empty);
        }

        /// <summary>
        ///     Methode die de press van de rotary encoder verwerkt.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnPress(object sender, EventArgs e)
        {
            if (_currentBarcode == null)
            {
                _ = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    if (DebugList.Items?.Count > 0) device.Send(CanCommand.Position, (Material)DebugList.Items[0]);
                });

                Task.Delay(10000).ContinueWith(task => device.Send(CanCommand.FillRateAll));
                return;
            }

            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                if (DebugList.Items?[0] == null) return;

                if (DebugList.Items[0] is Material mat)
                {
                    var product = new Product(_currentBarcode, mat);
                    _currentBarcode = null;
                    BarcodeInput.Text = "";
                    database.AddProduct(product);
                    List1.Items?.Insert(0, product);
                    if (List1.Items?.Count > 5) List1.Items?.RemoveAt(List1.Items.Count - 1);
                }
            });
        }

        /// <summary>
        ///     Methode die de positie's van de rotary encoder verwerkt.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPosition(object sender, Material e)
        {
            _ = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                if (DebugList.Items?.Count > 0) DebugList.Items.Clear();
                DebugList.Items?.Insert(0, e);
            });
        }

        /// <summary>
        ///     Methode die de doorgestuurde afstanden verwerkt.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnFill(object sender, (Material, float) e)
        {
            _ = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                var (material, fillRate) = e;
                SetFillGraph(material, fillRate);
            });
        }

        /// <summary>
        ///     Methode die wordt geactiveerd zodra we alle 5 de vakken hebben ontvangen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnFillAll(object sender, float[] e)
        {
            database.AddFill(e);
        } 
        #endregion
    }
}