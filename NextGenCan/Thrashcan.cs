﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace NextGenCan
{
    public class ThrashCan
    {
        private readonly float?[] _fillRate = new float?[5];
        private readonly CanConnection _connection;

        private int _calibrateDistance = 150;

        private Task _timerTask;

        public ThrashCan()
        {
            _connection = new CanConnection();
            _connection.OnMessage += OnMessage;
            _connection.Start();
        }

        public event EventHandler<Material> OnPosition;
        public event EventHandler<(Material, float)> OnFill;
        public event EventHandler<float[]> OnFillAll;
        public event EventHandler OnPress;

        public event EventHandler<string> OnBarcode;

        /// <summary>
        ///     Verwerking van de binnengekomen data naar nuttige events.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="msg"></param>
        private void OnMessage(object sender, string msg)
        {
            var args = msg.Split('.');
            int position;
            int distance;
            switch (args[0])
            {
                case "A":
                    int.TryParse(args[1], out position);
                    int.TryParse(args[2], out distance);
                    var fillRate = (_calibrateDistance - distance) / (float) _calibrateDistance;
                    OnFill?.Invoke(this, ((Material) position, fillRate));

                    _fillRate[position - 1] = fillRate;

                    var count = 0;
                    foreach (var fill in _fillRate)
                        if (fill == null)
                            count++;

                    if (count == 0) OnFillAll?.Invoke(this, _fillRate.Cast<float>().ToArray());
                    break;
                case "B":
                    if (ValidatePress(100)) OnPress?.Invoke(this, EventArgs.Empty);
                    break;
                case "C":
                    int.TryParse(args[1], out distance);
                    _calibrateDistance = distance;
                    break;
                case "R":
                    int.TryParse(args[1], out position);
                    OnPosition?.Invoke(this, (Material) position);
                    break;
            }
        }

        /// <summary>
        ///     Methode voor het stabiliseren van de inkomende knopdrukken.
        /// </summary>
        /// <param name="delay"></param>
        /// <returns></returns>
        private bool ValidatePress(int delay)
        {
            if (_timerTask == null || _timerTask.IsCompleted)
            {
                _timerTask = Task.Delay(delay);
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Methode om het barcode event vanuit MainPage te kunnen invoken.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="barcode"></param>
        internal void InvokeBarcode(object sender, string barcode)
        {
            OnBarcode?.Invoke(sender, barcode);
        }

        /// <summary>
        ///     Methode voor het versturen van commando's naar de prullenbak.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="arg"></param>
        public void Send(CanCommand command, Material arg = Material.Other)
        {
            switch (command)
            {
                case CanCommand.Position:
                    _connection.SendMessage("#D" + (int) arg + "%");
                    break;
                case CanCommand.FillRate:
                    _connection.SendMessage("#A" + (int) arg + "%");
                    break;
                case CanCommand.FillRateAll:
                    _connection.SendMessage("#U%");
                    break;
                case CanCommand.CalibrateAll:
                    _connection.SendMessage("#C%");
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(command), command, null);
            }
        }
    }
}