﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.SerialCommunication;
using Windows.Foundation;
using Windows.Storage.Streams;

namespace NextGenCan
{
    /// <summary>
    ///     Class die de connectie regelt met de prullenbak
    /// </summary>
    public class CanConnection : IDisposable
    {
        private DataReader _dataReader;

        private Task _listener;

        private SerialDevice _serialDevice;
        private bool _shouldRun;
        private IOutputStream _streamOut;

        /// <inheritdoc />
        public void Dispose()
        {
            _dataReader.Dispose();
            _streamOut.Dispose();
            _serialDevice.Dispose();
            _listener.Dispose();
        }

        /// <summary>
        ///     Event voor het ontvangen van data.
        /// </summary>
        public event EventHandler<string> OnMessage;

        /// <summary>
        ///     Methode voor het beginnen van de communicatie met de prullenbak.
        /// </summary>
        public void Start()
        {
            if (_serialDevice == null) GetStream("COM3");

            _listener = new Task(Listen);
            _listener.Start();
        }

        /// <summary>
        ///     Methode voor het versturen van een bericht naar de prullenbak.
        /// </summary>
        /// <param name="message"></param>
        public void SendMessage(string message)
        {
            var buffer = Encoding.UTF8.GetBytes(message).AsBuffer();
            _ = _streamOut.WriteAsync(buffer);
        }

        /// <summary>
        ///     Methode voor het opzetten van de communicatie met de prullenbak.
        /// </summary>
        /// <param name="port"></param>
        private void GetStream(string port)
        {
            var info = SerialDevice.GetDeviceSelector(port);
            var task = DeviceInformation.FindAllAsync(info);
            while (task.Status != AsyncStatus.Completed)
            {
            }

            var devices = task.GetResults();

            foreach (var item in devices)
            {
                if (item.Id == "" || item.IsEnabled == false) continue;

                var deviceTask = SerialDevice.FromIdAsync(item.Id);
                while (deviceTask.Status != AsyncStatus.Completed)
                {
                }

                var device = deviceTask.GetResults();
                if (device != null && device.PortName == port)
                {
                    device.WriteTimeout = TimeSpan.FromMilliseconds(1000);
                    device.ReadTimeout = TimeSpan.FromMilliseconds(1000);

                    _serialDevice = device;
                    _streamOut = _serialDevice.OutputStream;
                    _dataReader = new DataReader(_serialDevice.InputStream);
                }
            }
        }

        /// <summary>
        ///     Deze methode wordt in een apart Thread gedraaid, zodat er continu kan worden gekeken naar communicatie.
        /// </summary>
        private async void Listen()
        {
            _shouldRun = true;
            var buffer = new List<byte>();
            while (_shouldRun)
            {
                await _dataReader.LoadAsync(1);
                var data = _dataReader.ReadByte();
                buffer.Add(data);

                if (data != 10) continue;

                var array = buffer.ToArray();
                buffer.Clear();
                var msg = Encoding.UTF8.GetString(array);
                OnMessage?.Invoke(this, msg);
            }
        }
    }
}