﻿using System;
using MySql.Data.MySqlClient;

namespace NextGenCan
{
    /// <summary>
    ///     Class voor interactie met de database.
    /// </summary>
    public class Database
    {
        private readonly MySqlConnection _connection;

        public Database()
        {
            _connection =
                new MySqlConnection(
                    @"server=remotemysql.com;userid=VCW1ABbxGd;password=aUo6NtzXe7;database=VCW1ABbxGd");
        }

        /// <summary>
        ///     Voegt een barcode toe aan het logboek
        /// </summary>
        /// <param name="barcode"></param>
        public void AddBarcode(string barcode)
        {
            var connection = _connection.Clone();
            connection.Open();
            var command = connection.CreateCommand();

            command.CommandText = $"INSERT INTO logboek(barcode, tijd) VALUES('{barcode}',now())";
            command.ExecuteNonQuery();
            command.Dispose();
            connection.Close();
            connection.DisposeAsync();
        }

        /// <summary>
        ///     Verkrijgt alles wat we weten over een barcode
        /// </summary>
        /// <param name="barcode"></param>
        /// <returns></returns>
        public Product GetProduct(string barcode)
        {
            var connection = _connection.Clone();
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = $"SELECT barcode, naam, materiaal FROM producten WHERE barcode='{barcode}';";
            var reader = command.ExecuteReader();

            if (!reader.HasRows) return default;

            reader.Read();
            var name = reader.GetString("naam") ?? "";
            var materialName = reader.GetString("materiaal") ?? Material.Other.ToString();

            Enum.TryParse(materialName, out Material material);
            reader.Close();
            command.Dispose();
            connection.Close();
            connection.DisposeAsync();
            return new Product(barcode, material, name);
        }

        /// <summary>
        ///     Voegt een nieuw product toe aan de database van producten.
        /// </summary>
        /// <param name="product"></param>
        public void AddProduct(Product product)
        {
            var connection = _connection.Clone();
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText =
                $"INSERT INTO producten(barcode, naam, materiaal) VALUES('{product.Barcode}','{product.Name}','{product.Material.ToString()}')";
            command.ExecuteNonQuery();
            command.Dispose();
            connection.Close();
            connection.DisposeAsync();
        }

        /// <summary>
        ///     Voegt een nieuwe record toe met de volheid data van alle vakken
        /// </summary>
        /// <param name="fillRate"></param>
        public void AddFill(float[] fillRate)
        {
            var connection = _connection.Clone();
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText =
                $"INSERT INTO prullenbak(green, plastic, paper, deposit, other) VALUES({fillRate[(int) Material.Green - 1]},{fillRate[(int) Material.Plastic - 1]},{fillRate[(int) Material.Paper - 1]},{fillRate[(int) Material.Deposit - 1]},{fillRate[(int) Material.Other - 1]})";
            command.ExecuteNonQuery();
            command.Dispose();
            connection.Close();
            connection.DisposeAsync();
        }
    }
}