﻿namespace NextGenCan
{
    /// <summary>
    ///     Enum voor de verschillende mogelijke commando's die ik naar de prullenbak kan sturen.
    /// </summary>
    public enum CanCommand
    {
        /// <summary>
        ///     Commando voor naar een positie te gaan.
        /// </summary>
        Position,

        /// <summary>
        ///     Commando om volheid op te vragen.
        /// </summary>
        FillRate,

        /// <summary>
        ///     Commando om volheid van alles op te vragen.
        /// </summary>
        FillRateAll,

        /// <summary>
        ///     Calibratie voor de sensoren.
        /// </summary>
        CalibrateAll
    }
}