﻿using System;
using SkiaSharp;

// ReSharper disable InconsistentNaming
// ReSharper disable IdentifierTypo

namespace NextGenCan
{
    public enum Material
    {
        Green = 1,
        Plastic,
        Paper,
        Deposit,
        Other
    }

    /// <summary>
    ///     Centrale klas voor het gemakkelijk aanpassen van de kleuren die bij de categorieen horen.
    /// </summary>
    public static class MaterialColor
    {
        public static SKColor GetColor(Material material)
        {
            switch (material)
            {
                case Material.Plastic:
                    return SKColor.Parse("#ff0000");
                case Material.Paper:
                    return SKColor.Parse("#0051ff");
                case Material.Deposit:
                    return SKColor.Parse("#fffb00");
                case Material.Other:
                    return SKColor.Parse("#ffffff");
                case Material.Green:
                    return SKColor.Parse("#00ff22");
                default:
                    throw new ArgumentOutOfRangeException(nameof(material), material, null);
            }
        }
    }
}