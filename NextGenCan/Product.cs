﻿using System;

namespace NextGenCan
{
    public struct Product : IEquatable<Product>
    {
        public string Name { get; }
        public string Barcode { get; }
        public Material Material { get; }

        public Product(string barcode, Material material = Material.Other, string name = "")
        {
            Name = name;
            Barcode = barcode;
            Material = material;
        }

        public override string ToString()
        {
            if (Name == "") return Barcode + " " + Material;
            return Name + " " + Material;
        }

        /// <inheritdoc />
        public bool Equals(Product other)
        {
            return Barcode == other.Barcode;
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            return obj is Product other && Equals(other);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return Barcode != null ? Barcode.GetHashCode() : 0;
        }
    }
}